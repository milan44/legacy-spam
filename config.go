package main

import (
	"errors"
	"github.com/subosito/gotenv"
	"os"
	"strings"
)

var (
	BotToken    string
	GuildID     string
	LogChannel  string
	MutedRoleID string
	Action      string
	BanReason   string
)

func loadConfig() error {
	err := gotenv.Load(".env")
	if err != nil {
		return errors.New("failed to load .env")
	}

	BotToken = strings.TrimSpace(os.Getenv("BotToken"))
	GuildID = strings.TrimSpace(os.Getenv("GuildID"))
	LogChannel = strings.TrimSpace(os.Getenv("LogChannel"))
	MutedRoleID = strings.TrimSpace(os.Getenv("MutedRoleID"))
	Action = strings.TrimSpace(os.Getenv("Action"))
	BanReason = strings.TrimSpace(os.Getenv("BanReason"))

	if BotToken == "" {
		return errors.New("'BotToken' not set in .env")
	} else if LogChannel == "" {
		return errors.New("'LogChannel' not set in .env")
	} else if GuildID == "" {
		return errors.New("'GuildID' not set in .env")
	}

	switch Action {
	case "mute", "kick", "ban":
		// Do nothing
	default:
		Action = "mute"
		log.Warning("'Action' is not set or has an invalid value in .env, defaulting to 'mute'")
	}

	if Action == "ban" && BanReason == "" {
		return errors.New("'Action' is ban but 'BanReason' is not set in .env")
	} else if Action == "mute" && MutedRoleID == "" {
		return errors.New("'Action' is mute but 'MutedRoleID' is not set in .env")
	}

	return nil
}
