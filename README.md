# LegacySpam

A simple anti-spam bot for the LegacyRP discord.

### How does it work?

It keeps track of all messages that contain any link and were posted in the last minute. If the same message gets posted
in 3 or more channels, the bot automatically assigns the Muted role to that user and deletes the sent messages.

### Installation

1. Download the [Windows](dist/legacy-spam.exe) or [Linux](dist/legacy-spam) build
2. Download the [example.env](example.env) and rename it to `.env`
3. Fill out the required fields in the `.env`
   1. `BotToken` is the discord bot token for your instance
   2. `GuildID` is the guild id where the bot should detect spam in
   3. `LogChannel` is the channel id where logs should be sent in
   4. `Action` is the action that should be taken ("mute", "kick" or "ban")
   5. `MutedRoleID` is the id of the muted role that gets assigned (If `Action=mute`)
   6. `BanReason` is the reason added to the ban that gets handed out (If `Action=ban`)
4. Run the executable you downloaded :)

Make sure the bot has permissions to read all messages, send embeds to the log channel and manage roles, kick and ban as needed.