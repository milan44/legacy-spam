@echo off

echo Building Linux...
set GOOS=linux
go build -o dist/legacy-spam

echo Building Windows...
set GOOS=windows
go build -o dist/legacy-spam.exe