package main

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/milan44/logger"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	log = logger.NewGinStyleLogger(false)
)

func main() {
	rand.Seed(time.Now().UnixNano())

	log.Info("Loading Config...")
	err := loadConfig()
	log.MustPanic(err)

	log.Info("Connecting...")
	discord, err := discordgo.New("Bot " + BotToken)
	log.MustPanic(err)

	discord.AddHandler(OnMessage)
	discord.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages)

	err = discord.Open()
	log.MustPanic(err)

	err = discord.UpdateListeningStatus("Spam")
	if err != nil {
		log.Warning("Unable to update status: " + err.Error())
	}

	go CleanupCycle()

	log.Info("Startup completed.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	log.Warning("Disconnecting...")

	_ = discord.Close()
}
