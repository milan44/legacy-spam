package main

import (
	"github.com/bwmarrin/discordgo"
	"strings"
	"sync"
	"time"
)

const (
	MaxLifeTime = 1 * time.Minute
)

type MessageInfo struct {
	Counts      map[string][]string
	LastMessage time.Time
}

type UserInfo struct {
	LastMessage time.Time
	Messages    map[string]MessageInfo
}

var (
	InterestingMessages      = make(map[string]UserInfo)
	InterestingMessagesMutex sync.Mutex

	KeyWords = []string{
		"http://",
		"https://",
		"nitro",
		"@everyone",
		"skin",
		"cs:go",
		"leaving this fucking game",
	}
	KeyWordMutex sync.Mutex
)

func OnMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if s == nil || m == nil {
		return
	}

	if m.GuildID != GuildID {
		return
	}

	text := NormalizeContent(m.ContentWithMentionsReplaced())
	if !HasKeyWord(text) {
		return
	}

	now := time.Now()
	hash := MD5Hash(text)
	user := m.Author.ID

	InterestingMessagesMutex.Lock()
	info, ok := InterestingMessages[user]
	InterestingMessagesMutex.Unlock()

	if !ok {
		info = UserInfo{
			LastMessage: now,
			Messages: map[string]MessageInfo{
				hash: {
					Counts: map[string][]string{
						m.ChannelID: {m.ID},
					},
					LastMessage: now,
				},
			},
		}
	} else {
		msg, ok := info.Messages[hash]
		if !ok {
			msg = MessageInfo{
				Counts: map[string][]string{
					m.ChannelID: {m.ID},
				},
				LastMessage: now,
			}
		} else {
			if msg.Counts[m.ChannelID] == nil {
				msg.Counts[m.ChannelID] = make([]string, 0)
			}

			msg.Counts[m.ChannelID] = append(msg.Counts[m.ChannelID], m.ID)
			msg.LastMessage = now
		}

		if len(msg.Counts) >= 3 {
			FlagUser(s, m, hash, msg.Counts)
		}
	}

	InterestingMessagesMutex.Lock()
	InterestingMessages[user] = info
	InterestingMessagesMutex.Unlock()
}

func CleanupCycle() {
	for {
		time.Sleep(1 * time.Minute)

		now := time.Now()

		InterestingMessagesMutex.Lock()
		for id, user := range InterestingMessages {
			if now.Sub(user.LastMessage) > MaxLifeTime || len(user.Messages) == 0 {
				delete(InterestingMessages, id)
			} else {
				for i, msg := range user.Messages {
					if now.Sub(msg.LastMessage) > MaxLifeTime {
						delete(user.Messages, i)
					}
				}

				if len(user.Messages) == 0 {
					delete(InterestingMessages, id)
				}
			}
		}
		InterestingMessagesMutex.Unlock()
	}
}

func HasKeyWord(text string) bool {
	if text == "" {
		return false
	}

	KeyWordMutex.Lock()
	for _, word := range KeyWords {
		if strings.Contains(text, word) {
			KeyWordMutex.Unlock()
			return true
		}
	}
	KeyWordMutex.Unlock()

	return false
}
