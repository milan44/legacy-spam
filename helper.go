package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"regexp"
	"strings"
	"time"
)

func MD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func NormalizeContent(c string) string {
	rgx := regexp.MustCompile(`(?m)[^\w-:()/@]`)
	c = rgx.ReplaceAllString(c, "")

	return strings.ToLower(c)
}

func FlagUser(s *discordgo.Session, m *discordgo.MessageCreate, hash string, known map[string][]string) {
	log.Debug("Flagging user @" + m.Author.Username + "#" + m.Author.Discriminator + " for spam")

	err, deleted := DoAction(s, m)
	if err != nil {
		log.Warning("Failed to perform action '" + Action + "' for user " + m.Author.Username + "#" + m.Author.Discriminator + ". Make sure the bot has the required permissions to perform this action")
		log.WarningE(err)
	} else {
		_, err = s.ChannelMessageSendEmbed(LogChannel, &discordgo.MessageEmbed{
			Type:  "rich",
			Title: "Spam detected",
			Author: &discordgo.MessageEmbedAuthor{
				Name: hash + " - " + Action,
			},
			Description: "<@" + m.Author.ID + "> posted the same message containing keywords 3 or more times in different channels.\n```\n" + m.ContentWithMentionsReplaced() + "\n```",
			Timestamp:   time.Now().Format(time.RFC3339),
			Color:       5345212, // #518fbc
			Footer: &discordgo.MessageEmbedFooter{
				Text: "Bot made with ❤️ by Twoot",
			},
		})
		if err != nil {
			log.Warning("Failed to log spam user. Make sure the bot has permissions to send messages and embeds in the log channel")
			log.WarningE(err)
		}

		if !deleted {
			failed := 0
			for channel, msgs := range known {
				for _, msg := range msgs {
					err = s.ChannelMessageDelete(channel, msg)
					if err != nil {
						failed++
					}
				}
			}

			if failed > 0 {
				log.Warning(fmt.Sprintf("Failed to cleanup %d messages sent my user", failed))
				log.Debug("Make sure the bot has permissions to delete messages")
			}
		}
	}
}

func DoAction(s *discordgo.Session, m *discordgo.MessageCreate) (error, bool) {
	switch Action {
	case "mute":
		return s.GuildMemberRoleAdd(m.GuildID, m.Author.ID, MutedRoleID), false
	case "kick":
		return s.GuildMemberDelete(m.GuildID, m.Author.ID), false
	case "ban":
		return s.GuildBanCreateWithReason(m.GuildID, m.Author.ID, BanReason, 1), true
	}

	return nil, false
}
